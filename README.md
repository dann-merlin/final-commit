# Final-Commit

This is a simple game by Noah Lewis, Merlin Danner and Tobias Schwab.
As we were writing this game, we were still learning C++ and Game development, so all absurd parts of code shall be excused.

## What is it about?

You are a student who is currently working on a big project for one of his computer-science-courses. It is the day of the submission, but your files seem to have been scattered all over the Cyber-Space!
It is your goal to collect all the files and commit them before they are swallowed by the Cyber-Sea (also known as the GarbageCollector).
Once you have collected a file you will be able to commit it at a dedicated checkpoint. But watch out! There might be hidden bugs that need to be fixed before you can commit the file.

## How do I play it?

See the how-to-play menu in the main menu

## Questions?
Honestly just write an issue.

## License

Please note, that even though the Project is licensed under the GPLv3 License, this obviously can't apply to those files, that are licensed by another incompatible license.
Those other licenses are specified inside the correspending folder/file or in another simlarly named license-file.
